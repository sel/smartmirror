var Greeting = function() {
    this.update();

    setInterval(function() {
        window.greeting.update();
    }, 1000);
};

Greeting.prototype = {
    update: function() {
        var quoteElement = document.getElementById('quote'),
        
            timeDate = new Date(),
            h = timeDate.getHours(),
            m = timeDate.getMinutes(),

            // greeting = 'Good evening',
            quote = 'There was an unknown error,<br>but you still look lovely today';

        // Quote
        if (!quoteElement.innerHTML || (!h && !m)) {
            window.callback = function(data) {
                if (typeof data.quoteText != 'undefined')
                    quote = '<p>' + data.quoteText + '</p><span class="author">' + data.quoteAuthor + '</span>';
                else
                    quote = '<p>Quote server returned no quotes,<br>but you still look lovely today.</p>';

                quoteElement.innerHTML = quote;
            };

            var s = document.createElement('script');
            s.src = 'http://api.forismatic.com/api/1.0/?method=getQuote&format=jsonp&lang=en&jsonp=callback';
            document.body.appendChild(s);
        }

        // Greeting
        // if (h < 12)
        //     greeting = 'Good morning';
        // else if (h < 18)
        //     greeting = 'Good afternoon';

        // document.getElementById('greeting').innerHTML = greeting;
    }
};