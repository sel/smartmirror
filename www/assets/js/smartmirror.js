var SmartMirror = function() {};

SmartMirror.prototype = {
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

    json: function(param) {
        var request = new XMLHttpRequest();
            request.open('GET', param.url, true);

            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    // Success
                    var data = JSON.parse(request.responseText);

                    param.success(data);
                } else {
                    // We reached our target server, but it returned an error
                    param.statusError();
                }
            };

            request.onerror = function() {
                // There was a connection error of some sort
                param.connectionError();
            };

            request.send();
    }
};
 
document.addEventListener('deviceready', function() {
    window.smartMirror = new SmartMirror();

    window.timeDate = new TimeDate();
    window.greeting = new Greeting();
    window.weather = new Weather();
    window.transport = new Transport();

    window.callback = function() {};
    window.plugins.insomnia.keepAwake();
}, false);
