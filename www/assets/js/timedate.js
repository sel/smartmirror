var TimeDate = function() {
    this.update();
    
    setInterval(function() {
        window.timeDate.update();
    }, 1000);
};

TimeDate.prototype = {
    update: function() {
        var timeDate = new Date(),

            h = timeDate.getHours(),
            m = timeDate.getMinutes();

        // Time
        if (h < 10) h = '0' + h;
        if (m < 10) m = '0' + m;

        document.getElementById('time').innerHTML = h + ':' + m;

        // Date
        document.getElementById('date').innerHTML = window.smartMirror.dayNames[timeDate.getDay()] + ', ' + timeDate.getDate() + ' ' + window.smartMirror.monthNames[timeDate.getMonth()];
    }
};
