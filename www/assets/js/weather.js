var Weather = function() {
    this.update();

    setInterval(function() {
        window.weather.update();
    }, 15 * 60000);
};

Weather.prototype = {
    update: function() {
        var weatherElement = document.getElementById('weather');

        window.smartMirror.json({
            url: 'http://api.openweathermap.org/data/2.5/forecast?q=London,gb&units=metric&mode=json&appid=daaba9acdebfd7f1b82aa3cf070df37b',
            success: function(data) {
                if (data.cod == '200') {
                    data.list.forEach(function(item, index) {
                        if(index < 5) {
                            var date = new Date(item.dt * 1000),
                                h = date.getHours();

                            if (h < 10) h = '0' + h;

                            weatherElement.children[index].innerHTML =  '<p class="time"><span class="hour">' + h + '</span>00</p>';
                            weatherElement.children[index].innerHTML += '<p><i class="owf owf-' + item.weather[0].id + '"></i></p>';
                            weatherElement.children[index].innerHTML += '<p>' + Math.floor(item.main.temp) + '&deg</p>';
                        }
                    });
                } else {
                    // We reached our target server, but it returned an error
                }
            },
            statusError: function() {
            },
            connectionError: function() {
            }
        });
    }
};