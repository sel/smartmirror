var Transport = function() {
    this.update();

    setInterval(function() {
        window.transport.update();
    }, 60000);
};

Transport.prototype = {
    update: function() {
        var transportElement = document.getElementById('transport'),

            index = 0;

        window.smartMirror.json({
            url: 'https://api.tfl.gov.uk/line/mode/tube,overground,dlr/status',
            success: function(data) {
                data.forEach(function(item) {
                    if(item.id == 'dlr' || item.id == 'central' || item.id == 'northern' || item.id == 'london-overground') {
                        transportElement.children[index].children[0].innerHTML = item.name;
                        transportElement.children[index].children[1].innerHTML = item.lineStatuses[0].statusSeverityDescription;

                        index++;
                    }
                });
            },
            statusError: function() {
            },
            connectionError: function() {
            }
        });
    }
};